import React from 'react'
import { View, Text, StyleSheet } from 'react-native'


const ListItem = ({item}:any) => {

  const { nombre} = item

  return (
    <View style = {styles.container}>
      <Text>{nombre} </Text>
    </View>
  )
}

const styles = StyleSheet.create({

  container:{
    backgroundColor: '#D3D3D3',
    padding: 10,
    borderRadius: 10,
  }

})


export default ListItem