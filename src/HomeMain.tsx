import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import React from 'react'
import Home from './screens/Home'

const Stack = createNativeStackNavigator()

 const HomeMain = () => {
  return (
    <NavigationContainer>
        <Stack.Navigator>
            <Stack.Screen
                name = 'PRODUCTOS'
                component={Home}
            />
        </Stack.Navigator>
    </NavigationContainer>
  )
}

export default HomeMain