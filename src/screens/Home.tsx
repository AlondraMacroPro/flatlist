import { View, Text,FlatList, Modal, TextInput, TouchableOpacity, Button, Alert, StyleSheet } from 'react-native'
import React, { useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context'
import ListItem from '../components/ListItem'


const HomeScreen = () => {
  const [modalVisible,setModalVisible]= useState<boolean>(false)
  const [NewProducto, setNewProducto] = useState<string>('');
  
  const agregar = (val:string) => {
    setNewProducto(val)
    console.log(productos)
  }

  const [productos, setProductos] = useState([
    {
      id: '1',
      nombre: 'Celular',

    },
  ])

 
  function añadir(NewProducto:string) {
    setProductos ((prevProductos) =>{
      return[
        {nombre: NewProducto, id: Math.random().toString()},
        ...prevProductos
      ]
    })
  }

  return (
    <SafeAreaView style={{marginHorizontal: 10}}>
      <FlatList
        data = {productos} 
        renderItem = {({item}) => <ListItem item = {item}/>}
        ItemSeparatorComponent = {() => <View style={{marginBottom:10}}></View>}
        ListHeaderComponent = {() => <Text style ={{fontWeight: 'bold', fontSize:18, marginBottom:10, marginTop: 10}}> </Text>}
      />
     <View style={styles.button}>
        <Button
          title="Agregar"
          onPress={() => setModalVisible(true)} 
        />
      </View>
    <Modal
        animationType='slide'
        onDismiss={() => console.log('close')}
        onShow={() => console.log('show')}
        transparent
        visible={modalVisible}
      >
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(0, 0, 0.5)',
            justifyContent: 'center',
            alignItems: 'center'
          }}>
          <View
            style={{
              width: '60%',
              height: '50%',
              backgroundColor: '#D3D3D3',
            }}>             
              <TextInput
                onChangeText={agregar} 
                style={styles.input}
                placeholder="Producto"            
              />
            
            <View 
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                
              }}
            >
              <TouchableOpacity>
                <Button 
                  onPress={() => {
                        añadir(NewProducto)
                    Alert.alert('Producto agregado')
                    setModalVisible(false)
                  }}  

                  title="Agregar"
                 
               />
              </TouchableOpacity>
              <View >
              <TouchableOpacity>
                <Button
                  title="Cerrar"
                  onPress={() => setModalVisible(false)}
                     
                />
              </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    </SafeAreaView>  
  ) 
}

const styles = StyleSheet.create({
  button:{
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 100,
    paddingHorizontal: 50, 
    marginHorizontal: 100,
    
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    borderRadius: 10
  },
  
})
export default HomeScreen